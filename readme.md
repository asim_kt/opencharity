# Open Charity - A SCSS powered Drupal 7 theme
## About the project
Open charity is an organization that hosts events in London about open source software and solution for charities.

### Challenges
Open charity should be a clean theme without using any CSS frameworks at all. Though it’s difficult to implement from scratch considering the simplicity of the page importing a high weighted framework to the project is not necessary.

### Approach
The site should work in all latest browser versions including M.S Edge and IE11. That was a good thing since I can leverage the powerful CSS Flexbox properties.
And that was the plan.

I’ve started implementing the site using **CSS flexbox** from the top to bottom, for all layouts and alignments. It was simply easy to vertically align contents with dynamic items through flexbox, where it was a very hacky before flexbox. Even though the support is for latest browsers, I try to keep not to break all those layouts in older browsers too.

### BEM and SCSS
Of course, those extra brownie points! I’ve always a fan of preprocessors and CSS naming methodologies because in my career I always jump onto issues with naming classes and the maintenance nightmare for any CSS files. SCSS and BEM solved it all!

### Scaffolding and deploying
I had to implement the site in my free time after work. So I needed fast scaffolding and jump right into development. I used **Yeoman** for generating the basic files and **gulp task**. And it came with **bower** as the dependency manager. So I could concentrate on development without worrying minifying, concatenating, vender-prefixes, SCSS to CSS conversion and all.

For deploying and continuous integration I’ve used the most awesome **Netlify**. It comes with all powers every static site needs. I’ve planned to make it a Drupal theme as the last step so that I could create the site without any imperfections.


### To drupal 7
I've used **pantheon** for deploying the site at http://dev-open-charity.pantheonsite.io/. The theme files are in this repository at: opencharity-drupal7-theme-files.